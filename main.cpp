#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <ctime>
#include "utils/nmatrix.h"
#include "utils/dualdouble.h"
#include "searching_problems/searchalgorithms.h"
#include "searching_problems/problempuzzle8.h"
#include "searching_problems/puzzle8.h"//junk
#include "cryptosystems/rsa.h"//junk
#include <algorithm>    // std::reverse
#include <thread>

using namespace std;

# include "superlu/slu_ddefs.h"

int main();
double *cc_mv(int m, int n, int ncc, int icc[], int ccc[], double acc[],
		double x[]);
void cc_print(int m, int n, int ncc, int icc[], int ccc[], double acc[],
		string title);
void timestamp();


//****************************************************************************80

int main() {
    
    cout<<"************First example************"<<endl;
    DualDouble x(1.0, 1.0);
    DualDouble y(2.0, 0.0);
    cout<<"Dualdouble x:"<<x<<endl;
    cout<<"Dualdouble y:"<<y<<endl;
    DualDouble z = 3.0*x*x+2.0*x*y-y*y;
    cout<<"Function z= 3.0*x^2+2.0*x*y-y^2 evaluated in (x,y)=("<<x.get_val()<<" ,"<<y.get_val()<<")"<<endl;
    cout<<"Derivative with respect to x: "<<z.get_der()<<endl;
    x(1.0,0.0);
    y(2.0, 1.0);
    z = 3.0*x*x+2.0*x*y-y*y;
    cout<<"Derivative with respect to y: "<<z.get_der()<<endl;
    cout<<"************Second example************"<<endl;
    DualDouble x1(2.0, 1.0);
    DualDouble z1=cos(x1*x1*x1);
    cout<<"Function z1= cos(x1^3) evaluated in x1 = "<<x1.get_val()<<endl;
    cout<<"Derivative z1: "<<z1.get_der()<<endl;
    return 0;
}
//****************************************************************************80

double *cc_mv(int m, int n, int ncc, int icc[], int ccc[], double acc[],
		double x[])

//****************************************************************************80
//
//  Purpose:
//
//    CC_MV multiplies a CC matrix by a vector
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    15 July 2014
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Iain Duff, Roger Grimes, John Lewis,
//    User's Guide for the Harwell-Boeing Sparse Matrix Collection,
//    October 1992
//
//  Parameters:
//
//    Input, int M, the number of rows.
//
//    Input, int N, the number of columns.
//
//    Input, int NCC, the number of CC values.
//
//    Input, int ICC[NCC], the CC rows.
//
//    Input, int CCC[N+1], the compressed CC columns
//
//    Input, double ACC[NCC], the CC values.
//
//    Input, double X[N], the vector to be multiplied.
//
//    Output, double CC_MV[M], the product A*X.
//
		{
	double *b;
	int i;
	int j;
	int k;

	b = new double[m];

	for (i = 0; i < m; i++) {
		b[i] = 0.0;
	}

	for (j = 0; j < n; j++) {
		for (k = ccc[j]; k < ccc[j + 1]; k++) {
			i = icc[k];
			b[i] = b[i] + acc[k] * x[j];
		}
	}

	return b;
}
//****************************************************************************80

void cc_print(int m, int n, int ncc, int icc[], int ccc[], double acc[],
		string title)

//****************************************************************************80
//
//  Purpose:
//
//    CC_PRINT prints a sparse matrix in CC format.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    15 July 2014
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int M, the number of rows in the matrix.
//
//    Input, int N, the number of columns in the matrix.
//
//    Input, int NCC, the number of CC elements.
//
//    Input, int ICC[NCC], the CC rows.
//
//    Input, int CCC[N+1], the compressed CC columns.
//
//    Input, double ACC[NCC], the CC values.
//
//    Input, string TITLE, a title.
//
		{
	int i;
	int j;
	int jnext;
	int k;

	cout << "\n";
	cout << title << "\n";
	cout << "     #     I     J       A\n";
	cout << "  ----  ----  ----  --------------\n";
	cout << "\n";

	j = 0;
	jnext = ccc[1];

	for (k = 0; k < ncc; k++) {
		i = icc[k];
		while (jnext <= k) {
			j = j + 1;
			jnext = ccc[j + 1];
		}

		cout << setw(4) << k << "  " << setw(4) << i << "  " << setw(4) << j
				<< "  " << setw(16) << acc[k] << "\n";
	}

	return;
}
//****************************************************************************80

void timestamp()

//****************************************************************************80
//
//  Purpose:
//
//    TIMESTAMP prints the current YMDHMS date as a time stamp.
//
//  Example:
//
//    31 May 2001 09:45:54 AM
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    08 July 2009
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    None
//
{
# define TIME_SIZE 40

	static char time_buffer[TIME_SIZE];
	const struct std::tm *tm_ptr;
	size_t len;
	std::time_t now;

	now = std::time( NULL);
	tm_ptr = std::localtime(&now);

	len = std::strftime(time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm_ptr);

	std::cout << time_buffer << "\n";

	return;
# undef TIME_SIZE
}
