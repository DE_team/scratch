# README #

Please, read this document before getting to start.

### What is this repository for? ###

* Scratch is a repository for developing a miscellaneous of codes or subroutines that are developed as the project is called "from scratch".
* This conglomerate of codes includes different fields like algebra, A.I., cryptography and so on.
* Version: 0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Once the source files are downloaded, you should include your own .cpp and .h without modifying other's source file 
* unless bugs or fundamental upgrades are found. But first, adress yourself to the creator and come to an agreement 
* to perform the changes. You should include in the mains.txt (/Benchmarks) a small proof of use of your implementation.
* Once checked everything works all right git add it, commit it and push it. Thanks for your humble contributions.
 
### Who do I talk to? ###

* Daniel del Pozo
* Eva M. Andres-Lopez