/*
 * searchalgorithms.cpp
 *
 *  Created on: Dec 8, 2017
 *      Author: dani
 */
#include "searchalgorithms.h"
#include <iostream>
#include <deque>

template<typename T>
static bool isin(T s, std::deque<T> r) {
	bool isin = false;
	for (auto & n : r) {
		if (n->get_state() == s->get_state()) {
			isin = true;
			break;
		}
	}
	return isin;
}

void breadth_first(problem<std::string> const & prob) {
	//Create FIFO queues
	std::deque<nodesearch<std::string>*> frontier;
	std::deque<nodesearch<std::string>*> explored;


	nodesearch<std::string> * p1 = new nodesearch<std::string>(
			prob.get_initial_state());
	if (prob.goalTest(p1->get_state()))
		std::cout << "finished" << std::endl;
	frontier.push_back(p1);
	bool go_ = true;
	while (go_) {
		if(nodesearch<std::string>::counter > 300) go_=false;
		if (frontier.empty()) {
			std::cout << "Failure" << std::endl;
			break;
		}
		explored.push_back(frontier.front());
		nodesearch<std::string>* & node = frontier.front();

		frontier.pop_front();
		for (auto& x : prob.getActions(node->get_state())) { //for each action in possible actions for a certain state
			nodesearch<std::string> * child = prob.child(*node, x);
			if (!isin<nodesearch<std::string> *>(child, frontier)
					&& !isin<nodesearch<std::string> *>(child, explored)) {
				if (prob.goalTest(child->get_state())) {
					std::cout << "Solution reached: " << *child << std::endl;
					go_ = false;
					frontier.push_back(child);
					break; //affects for loop
				}
				frontier.push_back(child);
			} else {
				delete child;
			}
		}
	}
	for (auto & p : frontier) {
		delete p;
	}
	for (auto & p : explored) {
		delete p;
	}
}

void breadth_first(problem<puzzle8> const & prob) {
	//Create FIFO queues
	std::deque<nodesearch<puzzle8>*> frontier;
	std::deque<nodesearch<puzzle8>*> explored;


	nodesearch<puzzle8> * p1 = new nodesearch<puzzle8>(
			prob.get_initial_state());
	if (prob.goalTest(p1->get_state())) {
		std::cout << "finished" << std::endl;
		return;
	}
	frontier.push_back(p1);
	bool go_ = true;
	while (go_) {
		if(nodesearch<puzzle8>::counter > 300) go_=false;
		if (frontier.empty()) {
			std::cout << "Failure" << std::endl;
			break;
		}
		explored.push_back(frontier.front());
		nodesearch<puzzle8>* & node = frontier.front();

		frontier.pop_front();
		for (auto& x : prob.getActions(node->get_state())) { //for each action in possible actions for a certain state
			nodesearch<puzzle8> * child = prob.child(*node, x);
			if (!isin<nodesearch<puzzle8> *>(child, frontier)
					&& !isin<nodesearch<puzzle8> *>(child, explored)) {
				if (prob.goalTest(child->get_state())) {
					std::cout << "Solution reached: " << *child << std::endl;
					go_ = false;
					frontier.push_back(child);
					break; //affects for loop
				}
				frontier.push_back(child);
			} else {
				delete child;
			}
		}
	}
	std::cout<<"Number of nodesearch before desallocating dynamic memory: "<<nodesearch<puzzle8>::counter<<std::endl;
	for (auto & p : frontier) {
		delete p;
	}
	for (auto & p : explored) {
		delete p;
	}
	std::cout<<"Number of nodesearch after desallocating dynamic memory: "<<nodesearch<puzzle8>::counter<<std::endl;
}

