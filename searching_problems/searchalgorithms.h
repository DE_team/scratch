/*
 * searchalgorithms.h
 *
 *  Created on: Dec 8, 2017
 *      Author: dani
 */

#ifndef SCRATCH_SEARCHING_PROBLEMS_SEARCHALGORITHMS_H_
#define SCRATCH_SEARCHING_PROBLEMS_SEARCHALGORITHMS_H_
#include "problem.h"
#include "puzzle8.h"
void breadth_first(problem<std::string> const & prob);
void breadth_first(problem<puzzle8> const & prob);
#endif /* SCRATCH_SEARCHING_PROBLEMS_SEARCHALGORITHMS_H_ */
