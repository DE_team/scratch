/*
 * problemtrip.cpp
 *
 *  Created on: Nov 30, 2017
 *      Author: dani
 */
#include <algorithm>
#include <iostream>
#include <assert.h>
#include "problemtrip.h"
using namespace std;

const std::vector<std::string> problemtrip::getActions(
		const std::string & currentState) const {
	return net.at(currentState);
}

nodesearch<std::string>* problemtrip::child(const nodesearch<std::string> & p,
		std::string a) const {
//This function gives a new nodesearch object from a parent nodeserch p and an action.

//Check that the action is feasible for the parent state
	std::string parent_state = p.get_state();
	assert(
			std::find(getActions(parent_state).begin(),
					getActions(parent_state).end(), a)
					!= getActions(parent_state).end());
//If the action belongs to the set of actions of p, then return a new nodesearch that has as state
	//the action, and points to p.
	return new nodesearch<std::string>(a, p, a);
}
