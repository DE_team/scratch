/*
 * problemtrip.h
 *
 *  Created on: Nov 29, 2017
 *      Author: dani
 */

#ifndef SCRATCH_SEARCHING_PROBLEMS_PROBLEMTRIP_H_
#define SCRATCH_SEARCHING_PROBLEMS_PROBLEMTRIP_H_
#include "problem.h"
#include <map>
#include <vector>
#include <string>
typedef std::map<std::string, std::vector<std::string> > mapped_net;

class problemtrip: public problem<std::string> {
public:
	problemtrip(std::string init, std::string goal, const mapped_net& mn) :
			problem(init, goal), net(mn) {
	}
	~problemtrip(){};
	const std::vector<std::string> getActions(const std::string & currentState) const;
    nodesearch<std::string>* child(const nodesearch<std::string> & p, std::string a) const;
private:
	mapped_net net;
};

#endif /* SCRATCH_SEARCHING_PROBLEMS_PROBLEMTRIP_H_ */
