/*
 * problempuzzle8.h
 *
 *  Created on: Dec 10, 2017
 *      Author: dani
 */

#ifndef SCRATCH_SEARCHING_PROBLEMS_PROBLEMPUZZLE8_H_
#define SCRATCH_SEARCHING_PROBLEMS_PROBLEMPUZZLE8_H_
#include "problem.h"
#include <map>
#include <vector>
#include <string>
#include "puzzle8.h"

class problempuzzle8: public problem<puzzle8> {
public:
	problempuzzle8(const puzzle8 &init, const puzzle8 &goal) :
			problem(init, goal) {
	}
	~problempuzzle8() {
	}
	const std::vector<std::string> getActions(const puzzle8 & currentState) const;
	nodesearch<puzzle8>* child(const nodesearch<puzzle8> & p,std::string a) const;
};
//the problem knows the successors for a given state, puzzle8 has only the information of the state

#endif /* SCRATCH_SEARCHING_PROBLEMS_PROBLEMPUZZLE8_H_ */
