/*
 * nmatrix.cpp
 *
 *  Created on: Aug 31, 2017
 *      Author: dani
 */
#include "nmatrix.h"
#include <assert.h>
#include "superlu/slu_ddefs.h"
using namespace std;

nmatrix::nmatrix(size_t m, size_t n) :
rows(m), columns(n) {
    data = new double*[rows];
    double * foo = new double[rows * columns];
    for (size_t i = 0; i < rows; i++) {
        data[i] = foo + columns * i;
    }
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < columns; j++) {
            data[i][j] = 0.0;
        }
}

const double & nmatrix::operator()(size_t i, size_t j) const {
    return data[i][j];
}

double & nmatrix::operator()(size_t i, size_t j) {
    return data[i][j];
}

const nmatrix nmatrix::operator*(nmatrix const& B) {
    assert(columns == B.rows);
    nmatrix c(rows, B.columns);
    for (size_t i = 0; i < rows; i++) {
        for (size_t k = 0; k < B.columns; k++) {
            for (size_t j = 0; j < B.rows; j++) {
                c.data[i][k] += data[i][j] * B.data[j][k];
            }
        }
    }
    return c;
}

nmatrix::~nmatrix() {
    delete[] data[0];
    delete[] data;
}

void nmatrix::solveLinearSystem(double * & bx) const{
    //Data allocation
    SuperMatrix A;
    SuperMatrix B;
    SuperMatrix U;
    SuperMatrix L;
    double * acc;
    int info;
    int * icc;
    int * ccc;
    int *perm_c;
    int *perm_r;
    SuperLUStat_t stat;
    superlu_options_t options;
    
    int ncc=createCompressedColumnMatrix(*this, acc, icc,ccc);
    
    dCreate_CompCol_Matrix ( &A, rows, columns, ncc, acc, icc, ccc, SLU_NC, SLU_D, SLU_GE );
    
    
    dCreate_Dense_Matrix ( &B, rows, 1, bx, rows, SLU_DN, SLU_D, SLU_GE );
    
    //  Set space for the permutations.
    perm_r = new int[rows];
    perm_c = new int[columns];
    
    //  Set the input options.
    set_default_options ( &options );
    options.ColPerm = NATURAL;
    
    //  Initialize the statistics variables.
    StatInit ( &stat );
    
    
    //  Solve the linear system.
    dgssv ( &options, &A, perm_c, perm_r, &L, &U, &B, &stat, &info );
    
    
    
    delete perm_c;
    delete perm_r;
    Destroy_SuperMatrix_Store ( &A );
    Destroy_SuperMatrix_Store ( &B );
    Destroy_SuperNode_Matrix ( &L );
    Destroy_CompCol_Matrix ( &U );
    StatFree ( &stat );
    delete acc;
    delete icc;
    delete ccc;
    
}




int nmatrix::createCompressedColumnMatrix(nmatrix const & A, double * & acc,
                                          int * & icc, int * & ccc) const {
    int ncc=0; //number of non-zeroes
    for (size_t j = 0; j < A.columns; j++) {
        for (size_t i = 0; i < A.rows; i++) {
            if( !(A(i, j) == 0) ){
                ncc++;
            }
        }
    }
    
    acc = new double[ncc];
    icc = new int[ncc];
    ccc = new int[A.columns + 1];
    int index = 0;
    for (size_t j = 0; j < A.columns; j++) {
        for (size_t i = 0; i < A.rows; i++) {
            if (!(A(i, j) == 0)) {
                acc[index] = A(i, j);
                icc[index] = i;
                index++;
            }
        }
    }
    index = 0;
    int indexccc = 0;
    for (size_t j = 0; j < A.columns; j++) {
        bool first = true;
        for (size_t i = 0; i < A.rows; i++) {
            if (first && !(A(i, j) == 0)) {
                ccc[indexccc] = index;
                indexccc++;
            }
            
            if (!(A(i, j) == 0)) {
                index++;
                first = false;
            }
        }
    }
    ccc[indexccc ] = index ;
    return ncc;
}

std::ostream& operator<<(std::ostream & output, nmatrix const& z) {
    for (size_t i = 0; i < z.rows; i++) {
        output << "\n";
        for (size_t j = 0; j < z.columns; j++) {
            output << z(i, j) << " ";
        }
    }
    return output;
}

