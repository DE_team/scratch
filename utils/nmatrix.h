/*
 * nmatrix.h
 *
 *  Created on: Aug 31, 2017
 *      Author: dani
 */

#ifndef SCRATCH_UTILS_NMATRIX_H_
#define SCRATCH_UTILS_NMATRIX_H_
#include "nvector.h"
#include <iostream>

class nmatrix {
public:
    nmatrix(size_t m, size_t n);
    const double & operator()(const size_t i, const size_t j) const;
    double & operator()(const size_t i, const size_t j);
    ~nmatrix();
    const nmatrix operator*(nmatrix const& B);
    void solveLinearSystem(double * & bx) const;
    friend std::ostream& operator<<(std::ostream & output, nmatrix const& z);
    
private:
    nmatrix();//Should be non-existent?
    int createCompressedColumnMatrix(nmatrix const & A, double * & acc, int * & icc, int * & ccc) const;
    double **data;
    size_t rows;
    size_t columns;
};

#endif /* SCRATCH_UTILS_NMATRIX_H_ */
